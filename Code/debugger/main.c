#include <stdio.h>
#include <stdlib.h>

int main()
{
    int resultatActuel = 0;//r�sultat produit par la fonction de calcul
    int resultatAttendu = 0;//r�sultat que nous attendons et qui doit correspondre au r�sultat Actuel
    int operande1 = 10;
    int operande2 =  5;

    /*Addition*/
    printf("Le test pour l'addition commence...\n");
    resultatAttendu = 15;

    printf("Le calcul est :       %d + %d\n", operande1, operande2);
    printf("Le resultat attendu est:  %d\n", resultatAttendu);
    resultatActuel = addition(operande1, operande2);
    printf("Le resultat actuel est :  %d\n", resultatActuel);

    //tester le r�sultat pour valider le bon fonctionnement du programme
    if (resultatAttendu == resultatActuel)
    {
        printf("Le test pour l'addition se termine par un succes !\n");
    }
    else
    {
        printf("Le test pour l'addition se termine par une erreur!\n");
    }
    return 0;
}

int addition(int op1, int op2)
{
    int resultatTemp = 0;
    resultatTemp = op1 + op2;
    return resultatTemp;
}
