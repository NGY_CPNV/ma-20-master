/****************************************
*   Title   :   Menu                    *
*   Author  :   Nicolas Glassey         *
*   Version :   22-MAR-2018             *
****************************************/

#include <stdio.h>
#include <stdlib.h>


#define CHOICE_MIN 1
#define CHOICE_MAX 4



/** \brief Main - This function is designed to be the program's entry point
 *
 * \return 0 if no error. All others values mean trouble during last execution.
 *
 */
int main()
{
    int userChoice = -1;

    displayMenu();
    userChoice = askUser(CHOICE_MIN, CHOICE_MAX);

    while (userChoice == -1)
    {
        system("cls");
        printf("*** Input not available ***\n");
        displayMenu();
        userChoice = askUser(CHOICE_MIN, CHOICE_MAX);
    }

    manageUserChoice(userChoice);

    return 0;
}

/** \brief displayMenu - This function is designed to display the menu
 *
 * \return
 *
 */
void displayMenu()
{
    printf ("**********************************\n");
    printf ("***    1 - Help Me !           ***\n");
    printf ("***    2 - Start a battle      ***\n");
    printf ("***    3 - Display scores      ***\n");
    printf ("***    4 - Leave the NAVY      ***\n");
    printf ("**********************************\n");
}

/** \brief askUser - This function is designed to ask the user (about the menu)
 *  \param  choiceMin - the smaller limit
 *  \param  choiceMax - the highest limit
 *  \return user's input validated and then use as user's choice
 */
int askUser(int choiceMin, int choiceMax)
{
    int userInput = -1;
    int userChoiceValidated = -1;

    printf ("Make a choice : ");
    scanf("%d", &userInput);

    if (userInput >= choiceMin && userInput <= choiceMax)
    {
        userChoiceValidated = userInput;
    }

    return userChoiceValidated;
}

/** \brief manageUserChoice
 * \param  userChoice - user's choice
 * \return void
 */

void manageUserChoice(int userChoice)
{
    switch (userChoice)
    {
        case (1):
            printf("Choice 1\n");
            break;
        case (2):
            printf("Choice 2\n");
            break;
        case (3):
            printf("Choice 3\n");
            break;
        case (4):
            printf("Choice 4\n");
            break;
        default:
            printf("Choice unknown - default\n");
            break;
    }
}
