    /* ajouter ent�te*/
    /* ce main demanderait bien une refactoration en diverses fonctions (ouvrir, fermer fichier, �crire, lire,...)*/
    /* pour plus d'information...
    /* https://openclassrooms.com/courses/apprenez-a-programmer-en-c/lire-et-ecrire-dans-des-fichiers*/
    /* http://paristech.institutoptique.fr/site.php?id=469&fileid=2739*/

	#include<stdio.h> /*librairie C qui g�re les Input et Output (STDIO -> standard Input Output)*/

    #define TAILLE_LECTURE_MAX 100 /*limite la taille de la ligne qui sera lue dans le fichier*/

    /* ajouter une description de cette fonction*/
	int main()
	{

		FILE *pointeurSurMonFichier = NULL;//d�claration d'un pointeur qui me permettra d'acc�der � mon fichier. On l'initialise � "NULL".
		int x;//entier qui sera utilis� pour �crire une valeur dans mon fichier
		char chainePourLireMonFichier[TAILLE_LECTURE_MAX] = ""; // d�claration du cha�ne de caract�re qui me sera utile pour lire les donn�es. On l'initalise � "" (vide).


        /*Ouverture, �criture dans un fichier et fermeture du fichier*/
		pointeurSurMonFichier =fopen("output.txt", "w");//ouverture du fichier "output.txt" en mode �criture "w". Le fichier se trouvera au m�me "niveau" que l'executable

		if (pointeurSurMonFichier == NULL) //test. L'ouverture du fichier a-t-elle r�ussie ?
        {
            return 1; //en cas d'erreur, on retourne 1. Le programme s'arr�te.
        }

        //boucle permettant d'�crire les nombres 1 � 10 dans un fichier, en effectuant un retour � la ligne apr�s chaque �criture
		for (x=1; x<=10; x++)
        {
            fprintf(pointeurSurMonFichier,"%d\n", x);
        }

        //fermerture du fichier (tr�s important)
		fclose(pointeurSurMonFichier);


		/*Ouverture, lecture et affichage du contenu et fermeture du fichier*/
		pointeurSurMonFichier =fopen("output.txt", "r");//ouverture du fichier "output.txt" en mode lecture "r".

		if (pointeurSurMonFichier == NULL) //test. L'ouverture du fichier a-t-elle r�ussie ?
        {
            return 1; //en cas d'erreur, on retourne 1. Le programme s'arr�te.
        }

        while(fgets(chainePourLireMonFichier, TAILLE_LECTURE_MAX, pointeurSurMonFichier) != NULL)//on lit le fichier, ligne par ligne
        {
            printf("%s", chainePourLireMonFichier); //on affiche le contenu de la ligne
        }

        //fermerture du fichier (tr�s important)
		fclose(pointeurSurMonFichier);

		return  0;
	}

